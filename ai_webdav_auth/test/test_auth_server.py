import json
import ldap
import mock
import os
import socket
import shutil
import tempfile
import threading
import unittest

from ai_webdav_auth import auth_server
from ai_webdav_auth.client import DavAuthClient
from ai_webdav_auth.test import *


class DavAuthServerTestBase(StubBase, LdapTestBase):

    LDIFS = ['test-user.ldif']

    def setUp(self):
        super(DavAuthServerTestBase, self).setUp()
        self.tmpdir = tempfile.mkdtemp()
        self.socketpath = os.path.join(self.tmpdir, 'socket')
        srv = auth_server.DavAuthServer(self.socketpath, auth_server.DavAuthHandler)
        srv.ldap_params['uri'] = 'ldap://127.0.0.1:%d' % self.ldap_port
        srv_t = threading.Thread(target=srv.serve_forever)
        srv_t.setDaemon(True)
        srv_t.start()

        # The test LDAP server must always bind as the admin user, it
        # is not capable of letting clients authenticate against
        # arbitrary objects (unfortunately). This makes password
        # checks moot, as they will always succeed: in order to test
        # an authentication failure, stub out this method so that it
        # raises an ldap.INVALID_CREDENTIALS exception (see an example
        # in test_authenticate_bad_password below).
        def _admin_bind(conn, dn, pw):
            conn.simple_bind_s('cn=manager,o=Anarchy', 'testpass')
        self.stub(auth_server, 'ldap_bind', _admin_bind)

        # Force the local hostname to what's in LDAP.
        auth_server.DavAuthHandler.shard_id = TEST_HOST

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        super(DavAuthServerTestBase, self).tearDown()


class TestDavAuthServer(DavAuthServerTestBase):

    @mock.patch('ai_webdav_auth.auth_server.getpeercred', return_value=(1, 1, 1))
    def test_get_accounts_for_unknown_user(self, gpc):
        resp = DavAuthClient(self.socketpath).get_accounts()
        self.assertEqual({}, resp)

    @mock.patch('ai_webdav_auth.auth_server.getpeercred', return_value=(1, TEST_UID, 1))
    def test_get_accounts(self, gpc):
        resp = DavAuthClient(self.socketpath).get_accounts()
        expected = {
            '/dav/testdav': {
                'dn': TEST_DN,
                'ftpname': 'testdav',
                'home': '/home/users/investici.org/testdav',
            }}
        self.assertEqual(expected, resp)

    @mock.patch('ai_webdav_auth.auth_server.getpeercred', return_value=(1, TEST_UID, 1))
    @mock.patch('ai_webdav_auth.auth_server.pamh.authenticate', return_value=True)
    def test_authenticate_ok(self, gpc, pamh):
        resp = DavAuthClient(self.socketpath).authenticate(TEST_DN, TEST_PASS)
        self.assertEqual('ok', resp)

    @mock.patch('ai_webdav_auth.auth_server.ldap_bind', side_effect=ldap.INVALID_CREDENTIALS())
    @mock.patch('ai_webdav_auth.auth_server.getpeercred', return_value=(1, TEST_UID, 1))
    def test_authenticate_bad_password(self, gpc, ldapbind):
        resp = DavAuthClient(self.socketpath).authenticate(TEST_DN, 'wrong password')
        self.assertEqual('error', resp)

    @mock.patch('ai_webdav_auth.auth_server.getpeercred', return_value=(1, 1, 1))
    def test_authenticate_bad_uid(self, gpc):
        resp = DavAuthClient(self.socketpath).authenticate(TEST_DN, TEST_PASS)
        self.assertEqual('error', resp)



if __name__ == '__main__':
    unittest.main()
