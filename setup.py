#!/usr/bin/python3

from setuptools import setup, find_packages

setup(
    name="ai-webdav-auth",
    version="0.3",
    description="WebDAV auth server",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai3/tools/webdav-auth",
    install_requires=["python-ldap", "python-pam", "six"],
    setup_requires=[],
    packages=find_packages(),
    package_data={},
    entry_points={
        'console_scripts': [
          'ai-webdav-auth-server = ai_webdav_auth.auth_server:main',
          ],
        }
    )

