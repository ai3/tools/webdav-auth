webdav-auth
===

The authentication server bridges the
[users-dav](https://git.autistici.org/ai3/docker/apache2-users-dav)
container (which runs the
[webdav-server](https://git.autistici.org/ai3/tools/webdav-server)
component) and the local
[auth-server](https://git.autistici.org/id/auth).

It runs as a system-level daemon.

There are two issues with the way DAV authentication works in A/I:

* *authentication*: are username and password correct?
* *authorization*: can the user access this particular DAV path?

This is quite complicated by the fact that the DAV server process runs
in an isolated and untrusted container[^1], and we did not want to
just grant arbitrary LDAP access to anything in there. So we split the
authentication off to a process that runs *outside* the container, and
devised a simple restricted API just for the DAV server to talk to it
(safely, over a UNIX socket).

Here's how the process works in detail:

* when a DAV request comes in, Apache spawns a DAV server running with
  the appropriate user ID;
* when the DAV server starts, it fetches the list of DAV accounts
  associated with the user ID it is running as, and configures HTTP
  handlers for those paths only;
* on every DAV request, an authentication check is performed on the
  provided username/password, verifying also that the username appears
  in the list of DAV accounts associated with the current user ID.

Authorization is thusly provided by binding user IDs to specific DAV
accounts. The overall process is a bit convoluted, but it's convenient
to rely on Apache (specifically fcgid + suexec) to manage the setuid
processes.

The authentication server does not trust the DAV server, which is why
the user ID is not a request parameter but it is instead obtained by
looking at the *peer* of the UNIX socket connection.



[^1]: This used to be the case when we ran DAV and the users' PHP
    processes in the same environment, now that they are completely
    separate containers we could revisit this design decision.
