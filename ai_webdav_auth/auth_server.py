"""ai-webdav-auth-server

Simple authentication daemon for our WebDAV servers, that runs in an
isolated chroot and should not have the credentials to access LDAP
directly.

This server supports two kinds of requests (see dav.fcgi for the
client-side logic):

* given a UID, retrieve the known DAV accounts
* authenticate with UID/realm/username/password

Requests are sent through a UNIX socket, so that the server can know
the user ID of the process that made the request (the WebDAV FastCGI
runner).
"""

import contextlib
import grp
import json
import ldap
import ldap.dn
import logging
import logging.handlers
import optparse
import os
import pam
import pwd
import socket
import socketserver
import struct

from io import TextIOWrapper

# Taken from <sys/socket.h>. Python 2 does not have this constant.
SO_PEERCRED = 17

# Default LDAP URI.
LDAP_DEFAULT_URI = 'ldapi://%2fvar%2frun%2fldap%2fldapi'

# LDAP base UID.
LDAP_DEFAULT_BASE_UID = 'ou=People, dc=investici, dc=org, o=Anarchy'

# Service used for PAM.
PAM_SERVICE = 'dav'


pamh = pam.pam()


def getpeercred(sock):
    # This is Linux-specific.
    data = sock.getsockopt(socket.SOL_SOCKET, SO_PEERCRED, struct.calcsize('3i'))
    return struct.unpack('3i', data)


def ldap_bind(conn, dn, pw):
    # Separated for easier testing.
    conn.simple_bind_s(dn, pw)


class DavAuthServer(socketserver.ThreadingUnixStreamServer):

    ldap_params = {
        'uri': LDAP_DEFAULT_URI,
        'base_uid': LDAP_DEFAULT_BASE_UID,
        'bind_dn': None,
        'bind_pw': None,
    }

    @contextlib.contextmanager
    def ldapconn(self, bind_dn=None, bind_pw=None):
        c = ldap.initialize(self.ldap_params['uri'])
        try:
            if bind_dn:
                ldap_bind(c, bind_dn, bind_pw)
            elif self.ldap_params.get('bind_dn'):
                ldap_bind(c, self.ldap_params['bind_dn'], self.ldap_params['bind_pw'])
            yield c
        finally:
            c.unbind()


class DavAuthHandler(socketserver.StreamRequestHandler):
    shard_id = None

    def handle(self):
        pid, uid, gid = getpeercred(self.request)
        request_debug_str = ''
        try:
            request = json.loads(TextIOWrapper(self.rfile, 'utf-8').readline())
            if request['type'] == 'auth':
                request_debug_str = 'auth(dn=%s)' % request['dn']
                response = self.authenticate(uid, request['dn'], request['password'])
            elif request['type'] == 'get_accounts':
                request_debug_str = 'get_accounts()'
                response = self.get_accounts(uid)
            else:
                raise ValueError('Unknown request type')
            response_data = json.dumps(response)
            self.wfile.write(response_data.encode('utf-8'))
        except Exception as e:
            response_data = f'unhandled exception: {e}'
        logging.info('request from uid=%d gid=%d pid=%d: %s -> %s',
                     uid, gid, pid,
                     request_debug_str, response_data)

    def authenticate(self, uid, dn, password):
        # An empty password causes a "server is unwilling to perform"
        # error, so we might just as well handle it now.
        if not password:
            return 'error'
        try:
            # Check the account exists on this host and has the right
            # UID.  This is necessary so you can't log in to other
            # people's repositories!
            with self.server.ldapconn() as l:
                result = l.search_s(dn, ldap.SCOPE_BASE,
                        '(&(host=%s)(uidNumber=%d))' % (self.shard_id, uid))
                if len(result) == 0:
                    logging.error('account for %s uid %d not found on %s', dn,
                            uid, self.shard_id)
                    return 'error'

            # Defer the actual authentication to PAM, so we don't have
            # to actually check the password ourselves. Get the
            # username from the dn (ftpname=).
            username = ldap.dn.explode_dn(dn)[0][len("ftpname="):]
            if pamh.authenticate(username, password, service=PAM_SERVICE):
                return 'ok'
            logging.error('authentication failure for %s (uid=%d)', dn, uid)
            return 'error'
        except ldap.NO_SUCH_OBJECT:
            logging.error('cannot find object for %s (uid=%d)', dn, uid)
            return 'error'
        # Let other errors pass through and get caught (and logged) by the generic
        # exception handler one level up.
        return 'error'

    def get_accounts(self, uid):
        with self.server.ldapconn() as l:
            accounts = {}
            result = l.search_s(
                self.server.ldap_params['base_uid'], ldap.SCOPE_SUBTREE,
                '(&(objectClass=ftpAccount)(status=active)(uidNumber=%d)(host=%s))' % (uid, self.shard_id),
                ['ftpname', 'homeDirectory'])
            for r in result:
                name = r[1]['ftpname'][0].decode('utf-8')
                realm = '/dav/' + name
                accounts[realm] = {'dn': r[0],
                                   'ftpname': name,
                                   'home': r[1]['homeDirectory'][0].decode('utf-8')}
            return accounts


def run_server(socket_path, username, group, shard_id):
    # Set a permissive umask for the (eventual) mkdir.
    old_umask = os.umask(0o022)

    # Create the socket directory if it does not exist.
    socket_dir = os.path.dirname(socket_path)
    if not os.path.exists(socket_dir):
        os.makedirs(socket_dir, 0o755)

    # Delete the socket if it already exists.
    if os.path.exists(socket_path):
        os.unlink(socket_path)

    # Set the umask appropriately so that the socket has mode 777.
    os.umask(0)
    server = DavAuthServer(socket_path, DavAuthHandler)
    os.umask(old_umask)
    if username and group:
        drop_privs(username, group)
    server.serve_forever()


def drop_privs(username, group):
    if os.getuid() != 0:
        return

    userobj = pwd.getpwnam(username)
    grpobj = grp.getgrnam(group)

    os.setgid(grpobj.gr_gid)
    os.initgroups(username, grpobj.gr_gid)
    os.setuid(userobj.pw_uid)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--ldap-uri', dest='ldap_uri', default=LDAP_DEFAULT_URI,
                      metavar='URI', help='LDAP URI connection string')
    parser.add_option('--ldap-bind-dn', dest='ldap_bind_dn',
                      metavar='DN', help='LDAP bind DN')
    parser.add_option('--ldap-bind-pw', dest='ldap_bind_pw',
                      metavar='PASS', help='LDAP bind password')
    parser.add_option('--ldap-bind-pwfile', dest='ldap_bind_pwfile',
                      metavar='FILE', default='/etc/ldap.secret',
                      help='Read LDAP bind password from this file')
    parser.add_option('--ldap-base-uid', dest='ldap_base_uid',
                      metavar='DN', default=LDAP_DEFAULT_BASE_UID,
                      help='Default LDAP base DN')
    parser.add_option('--shard-id', dest='shard_id',
                      metavar='ID', default=None,
                      help='Consider accounts for this shard id')
    parser.add_option('--user',
                      help='Run as a different user')
    parser.add_option('--group',
                      help='Run as a different group')
    parser.add_option('--socket', dest='socket_path',
                      default='/var/run/authdav/auth', metavar='PATH',
                      help='Location of the listening UNIX socket')
    opts, args = parser.parse_args()
    if len(args) != 0:
        parser.error('Too many arguments')

    # Set LDAP params.
    if opts.ldap_uri:
        DavAuthServer.ldap_params['uri'] = opts.ldap_uri
    if opts.ldap_base_uid:
        DavAuthServer.ldap_params['base_uid'] = opts.ldap_base_uid
    if opts.ldap_bind_dn:
        DavAuthServer.ldap_params['bind_dn'] = opts.ldap_bind_dn
        if opts.ldap_bind_pw:
            DavAuthServer.ldap_params['bind_pw'] = opts.ldap_bind_pw
        elif opts.ldap_bind_pwfile:
            with open(opts.ldap_bind_pwfile) as fd:
                DavAuthServer.ldap_params['bind_pw'] = fd.read().strip()
        else:
            parser.error('You must specify a bind password')

    if opts.shard_id:
        DavAuthHandler.shard_id = opts.shard_id

    # Configure syslog logging.
    handler = logging.handlers.SysLogHandler(
        address='/dev/log',
        facility=logging.handlers.SysLogHandler.LOG_DAEMON)
    handler.setFormatter(
        logging.Formatter('auth_server_dav: %(message)s'))
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    root_logger.addHandler(handler)

    run_server(opts.socket_path, opts.user, opts.group, opts.shard_id)


if __name__ == '__main__':
    main()
